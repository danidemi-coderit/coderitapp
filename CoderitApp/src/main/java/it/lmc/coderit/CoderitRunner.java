package it.lmc.coderit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import it.lmc.coderit.model.Dirigente;
import it.lmc.coderit.model.Manager;
import it.lmc.coderit.model.Tecnico;
import it.lmc.coderit.service.LavoratoreService;
@Component
public class CoderitRunner implements CommandLineRunner {
	@Autowired
	LavoratoreService service;

	@Override
	public void run(String... args) throws Exception {
		Dirigente d1= Dirigente.builder().withNome("Anna").withCognome("Arnoldi").build();
		Dirigente d2= Dirigente.builder().withNome("Beniamino"). withCognome("Brumotti").build();
		Manager m1= Manager.builder().withNome("Emilia").withCognome("Eberini").build();
		Manager m2= Manager.builder().withNome("Carla").withCognome("Cattaneo").withManager(m1).build();
		Manager m3=Manager.builder().withNome("Dario").withCognome("Dionigi").build();
		Tecnico t1= Tecnico.builder().withNome("Franco").withCognome("Fastoni").withAnzianita(5).withManager(m2).build();
		Tecnico t2=Tecnico.builder().withNome("Giorgia").withCognome("Grigioni").withAnzianita(2).withManager(m2).build();
		Tecnico t3=Tecnico.builder().withNome("Hector").withCognome("Hernandez").withAnzianita(2).withManager(m2).build();
		Tecnico t4= Tecnico.builder().withNome("Ivana").withCognome("Iudice").withAnzianita(12).withManager(m3).build();
		Tecnico t5= Tecnico.builder().withNome("Ludovico").withCognome("Landini").withAnzianita(14).withManager(m3).build();
		Tecnico t6= Tecnico.builder().withNome("Maria").withCognome("Maiolica").withAnzianita(7).withManager(m3).build();
		
		
		
		System.out.println(service.saveDirigente(d1));
		service.saveDirigente(d2);
		System.out.println(service.saveManager(m1));
		service.saveManager(m2);
		System.out.println(service.saveManager(m3));
		service.saveTecnico(t1);
		service.saveTecnico(t2);
		service.saveTecnico(t3);
		service.saveTecnico(t4);
		service.saveTecnico(t5);
		service.saveTecnico(t6);
		service.findAll();
		System.out.println("La somma totale degli stipendi e': "+ service.countStipendi());
		
		
	}

}
