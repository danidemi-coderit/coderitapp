package it.lmc.coderit.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.lmc.coderit.model.Manager;

public interface ManagerRepository extends JpaRepository<Manager, Long>{
	@Query("select m from Manager m where m.nome=:nome and m.cognome=:cognome")
	public Optional<Manager> findByNomeAndCognome(String nome, String cognome);

}
