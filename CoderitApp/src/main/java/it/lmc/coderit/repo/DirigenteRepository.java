package it.lmc.coderit.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.lmc.coderit.model.Dirigente;

public interface DirigenteRepository extends JpaRepository<Dirigente, Long> {
	@Query("Select d.bonus from Dirigente d group by d.bonus")
	public List<Long>findStipendio();

}
