package it.lmc.coderit.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.lmc.coderit.model.Manager;
import it.lmc.coderit.model.Tecnico;

public interface TecnicoRepository extends JpaRepository<Tecnico, Long> {
	public List<Tecnico> findAllByManager(Manager manager);
}
