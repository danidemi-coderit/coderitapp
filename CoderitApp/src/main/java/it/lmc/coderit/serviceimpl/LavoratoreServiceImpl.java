package it.lmc.coderit.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.lmc.coderit.model.Dirigente;
import it.lmc.coderit.model.Manager;
import it.lmc.coderit.model.Tecnico;
import it.lmc.coderit.repo.DirigenteRepository;
import it.lmc.coderit.repo.ManagerRepository;
import it.lmc.coderit.repo.TecnicoRepository;
import it.lmc.coderit.service.LavoratoreService;

@Service
public class LavoratoreServiceImpl implements LavoratoreService {
	@Autowired
	TecnicoRepository tecnicoRepo;
	@Autowired
	ManagerRepository managerRepo;
	@Autowired
	DirigenteRepository dirigenteRepo;

	@Override
	public Long calcoloBonusManager(Manager manager) {
		List<Tecnico> tecnici = tecnicoRepo.findAllByManager(manager);
		if(tecnici.size()>0) {
		Long sum = tecnici.stream().map(Tecnico::getStipendio).mapToLong(Long::valueOf).sum();
		return (sum * 5) / 100;
		}else
			return 0l;
	}

	@Override
	public Long calcoloBonusDirigenti(Manager m) {
		
		/*Long sumManager = managerRepo.findAll().stream().map(Manager::getStipendio).mapToLong(Long::valueOf).sum();
		Long percMan = (sumManager * 10) / 100;*/
		Long sumTecnici = tecnicoRepo.findAll().stream().map(Tecnico::getStipendio).mapToLong(Long::valueOf).sum();
		Long percTec = (sumTecnici * 10) / 100;
		Long stipendio;
		//return percMan + percTec;
		if(dirigenteRepo.findStipendio().size()>1) {
		stipendio= dirigenteRepo.findStipendio().get(0);
		}else {
			stipendio= dirigenteRepo.findStipendio().get(0)+(m.getStipendio()*5)/100;
			}
		return stipendio+percTec;
	}

	@Override
	public void aggiornaDirigenti(Manager m) {
		List<Dirigente> dirigenti = dirigenteRepo.findAll();
		dirigenti.stream().forEach(d -> {
			d.setBonus(calcoloBonusDirigenti(m));
			d.setStipendio(calcoloBonusDirigenti(m));
			dirigenteRepo.save(d);
		});

	}

	@Override
	public void aggiornaManager(Manager manager) {
		managerRepo.save(manager.setBonus(calcoloBonusManager(manager)).setStipendio());

	}

	@Override
	public String saveTecnico(Tecnico tecnico) {
		try {
			Optional<Manager> manager = managerRepo.findByNomeAndCognome(tecnico.getManager().getNome(),
					tecnico.getManager().getCognome());
			if (manager.isPresent()) {
				tecnico.setStipendio(tecnico.getAnzianita() < 10 ? 1500l : 1600l);
				tecnicoRepo.save(tecnico);
				aggiornaManager(manager.get());
				aggiornaDirigenti(manager.get());
				return "Tecnico salvato correttamente";
			} else
				return "Manager non trovato";

		} catch (Exception e) {
			return e.getMessage();
		}
	}

	@Override
	public String saveManager(Manager manager) {
		try {
			if (manager.getManager()==null) {
				managerRepo.save(manager.setStipendio());
				aggiornaDirigenti(manager);
			}

			else {
				managerRepo.save(manager.setStipendio().setBonus(calcoloBonusManager(manager)));
				Optional<Manager> m = managerRepo.findByNomeAndCognome(manager.getManager().getNome(),
						manager.getManager().getCognome());
				if(m.isPresent()){
					aggiornaManager(m.get());
					aggiornaDirigenti(m.get());
				}
				aggiornaDirigenti(manager);
			}

			return "Manager salvato";
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public String saveDirigente(Dirigente dirigente) {
		try {
			dirigente.setStipendio(0L);
			dirigenteRepo.save(dirigente);

			return "Dirigente salvato correttamente";
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public void findAll() {
		try {
			tecnicoRepo.findAll().stream().forEach(t -> System.out.println(t.toString()));
			managerRepo.findAll().stream().forEach(m -> System.out.println(m.toString()));
			dirigenteRepo.findAll().stream().forEach(d -> System.out.println(d.toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Long countStipendi() {
		try {
			Long a = tecnicoRepo.findAll().stream().map(t -> t.getStipendio()).mapToLong(Long::valueOf).sum();
			Long b = managerRepo.findAll().stream().map(t -> t.getStipendio()).mapToLong(Long::valueOf).sum();
			Long c = dirigenteRepo.findAll().stream().map(t -> t.getStipendio()).mapToLong(Long::valueOf).sum();
			return a + b + c;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
