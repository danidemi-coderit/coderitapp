package it.lmc.coderit.service;

import it.lmc.coderit.model.Dirigente;
import it.lmc.coderit.model.Manager;
import it.lmc.coderit.model.Tecnico;


public interface LavoratoreService {
	public Long calcoloBonusManager(Manager manager);
	public Long calcoloBonusDirigenti(Manager m);
	public String saveTecnico(Tecnico tecnico);
	public String saveManager(Manager manager);
	public void aggiornaDirigenti(Manager m);
	public void aggiornaManager(Manager manager);
	public void findAll();
	public String saveDirigente(Dirigente dirigente);
	public Long countStipendi();
}
