package it.lmc.coderit;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoderitAppApplication {
	public static void main(String[] args) {
		SpringApplication.run(CoderitAppApplication.class, args);
		
	}

}
