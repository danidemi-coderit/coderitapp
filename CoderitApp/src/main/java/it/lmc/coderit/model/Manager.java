package it.lmc.coderit.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.Nullable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
public class Manager extends Lavoratore {
	
	@Value("${manager}")
	private Long stipendioBase;
	private Long bonus;
	private Long stipendio;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)

	private Manager manager;
	
	@Builder(setterPrefix = "with")
	public Manager(@NonNull String nome, @NonNull String cognome, Manager manager) {
		super(nome, cognome);
		this.manager=manager;
		this.stipendioBase=2000l;
	}
	
	public Manager setBonus(Long bonus) {
		this.bonus=0+bonus;
		return this;
	}
	
	public Manager setStipendio() {
		if(bonus==null) {
			this.stipendio=2000l;
		}else
		this.stipendio=2000l+bonus;
		return this;
	}

	
	
	
	
}
