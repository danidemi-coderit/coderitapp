package it.lmc.coderit.model;

import javax.persistence.Entity;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Value;


import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper =true)
@Entity
public class Dirigente extends Lavoratore {

	@NonNull
	@Value("${dirigente}")
	private static Long stipendioBase;
	private Long bonus;
	@NonNull
	private Long stipendio;
	
	
	@Builder(setterPrefix = "with")
	public Dirigente(@NonNull String nome, @NonNull String cognome) {
		super(nome, cognome);
		this.bonus=0l;
	}
	
	public Dirigente setBonus() {
		this.bonus=0+bonus;
		return this;
	}
	public void setStipendio(Long bonus) {
		this.stipendio=2500l+bonus;
	}



	
	
	
	
	
}
