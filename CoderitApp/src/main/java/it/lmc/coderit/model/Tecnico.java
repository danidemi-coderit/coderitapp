package it.lmc.coderit.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Value;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)


public class Tecnico extends Lavoratore {
	


	private Long stipendio;
	@ManyToOne
	private Manager manager;
	private Integer anzianita;
	
	@Value("${low}")
	private Long lower;
	
	@Value("${high}")
	private Long higher;
	
	
	/**
	 * Nel momento in cui si istanzia un tecnico, lo stipendio è calcolato automaticamente tramite l'anzianita immessa. 
	 * @param anzianita
	 */
	@Builder(setterPrefix = "with")
	public Tecnico(String nome, String cognome, Manager manager, int anzianita) {
		super(nome, cognome);
		this.manager = manager;
		this.anzianita = anzianita;
		this.stipendio =(anzianita<10 ? 1500l : 1600l);
		
	}
	
	
	public Long getStipendio() {
		return stipendio;
	}
	
	
	
	
	
}
